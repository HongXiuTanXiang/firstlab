#ifndef CircleList_hpp
#define CircleList_hpp

#include <iostream>
using namespace std;

class Node
{
  public:
    Node *next;
    int data;
};

class CircleList
{
  private:
    Node *head;
    int length;

  public:
    CircleList()
    {
        head = new Node();
        head->next = head;
        head->data = 0;
        length = 0;
    }
    ~CircleList()
    {
        delete (head);
    }
    void traverseNode();              //遍历链表
    void deleteNode(int n);           //删除位置为n的结点
    void insertNode(int n, int data); //在制定位置插入结点
    int getLength();                  //得到链表长度
    bool isEmpty();                   //判断链表是否为空
};

void CircleList:: traverseNode(){
    Node *p = head->next;
    while(p->next != head){
        cout<<p->data<<endl;
        p = p->next;
    }
    
}

void CircleList::insertNode(int n, int data){
    Node *p = head;
    int count = 0;
    while(p->next != head && count < n-1){
        p = p->next;
        count++;
    }

    if (p->next == head) { //p是尾节点,在尾部插入
        Node *s = new Node();
        s->data = data;
        s->next = head;
        p->next = s;
        length++;
    } else { //否则在其他位置插入,p 为n-1位置
        Node *temp = p->next;
        Node *s = new Node();
        s->data = data;
        s->next = temp;
        p->next = s;
        length++;
    }
    
}

void CircleList::deleteNode(int n){

    Node *p = head;
    int count = 0;
    while (p->next != head && count < n - 1)
    {
        p = p->next;
        count++;
    }

    if (p->next == head) {
        throw "location";
    } else if (n < length) {
        
        Node *temp = p->next;
        p->next == temp->next;
        delete temp;
        length--;
    } else if (n == length) {
        Node *temp = p->next;
        p->next == head;
        delete temp;
        length--;
    }
    
}

#endif