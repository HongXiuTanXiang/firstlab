#ifndef CreateBiTree_hpp
#define CreateBiTree_hpp

#include <stdio.h>
#include <stdlib.h>

typedef struct BitNode
{
    char data;
    struct BitNode *lchild,*rchild;
}BitNode,*BitTree;

///创建一棵二叉树，约定用户遵照前序遍历的方式输入数据
void createBitTree(BitTree *tree){

    char c;
    scanf("%c",&c);

    if (' ' == c)
    {
        *tree = NULL;
    } else {
        *tree = (BitNode*)malloc(sizeof(BitNode));
        (*tree)->data = c;
        createBitTree(&((*tree)->lchild));
        createBitTree(&((*tree)->rchild));
    }
    
}

void visit(char c,int level){
    printf("%c 位于第%d层 \n",c,level);
}

void preOrderTraverse(BitTree tree,int level){
    if (tree) {
        visit(tree->data,level);
        preOrderTraverse(tree->lchild,level+1);
        preOrderTraverse(tree->rchild,level+1);
    }
    
}

void test_bit_tree(){
    int level = 1;
    BitTree tree = NULL;
    createBitTree(&tree);
    preOrderTraverse(tree, level);
}


#endif