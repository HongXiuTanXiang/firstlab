#ifdef KmpSearch_hpp
#define KmpSearch_hpp
#include <iostream>
using namespace std;

//https://www.cnblogs.com/zhangtianq/p/5839909.html

#include <stdio.h>
typedef String char *;

void GetNext(char *p, int next[])
{
    int pLen = strlen(p);
    next[0] = -1;
    int k = -1;
    int j = 0;
    while (j < pLen - 1)
    {
        //p[k]表示前缀，p[j]表示后缀
        if (k == -1 || p[j] == p[k])
        {
            ++k;
            ++j;
            next[j] = k;
        }
        else
        {
            k = next[k];
        }
    }
}

// 返回子串T在主串S第pos个字符之后的位置
// 若不存在，则返回0
int Index_KMP(String S, String T, int pos)
{
    int i = pos;
    int j = 1;
    int next[255];

    GetNext(T, next);

    while (i <= S[0] && j <= T[0])
    {
        if (0 == j || S[i] == T[j])
        {
            i++;
            j++;
        }
        else
        {
            j = next[j];
        }
    }

    if (j > T[0])
    {
        return i - T[0];
    }
    else
    {
        return 0;
    }
}

#endif