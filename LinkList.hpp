#ifndef LinkList_hpp
#define LinkList_hpp

#include <iostream>
using namespace std;

template <class DataType>
struct Node
{
    DataType data;
    Node<DataType> *next;
};

template <class DataType>
class LinkList
{
private:
    Node<DataType> *first;
public:
    LinkList(/* args */);
    ~LinkList();

    LinkList(DataType a[],int n);
    int Length();
    DataType Get(int i);
    int Locate(DataType x);
    void Insert(int i, DataType x);
    DataType Delete(int i);
    void PrintList();
};

template <class DataType>
LinkList<DataType>::LinkList(/* args */)
{
    this->first = new Node<DataType>;
    first->next = NULL;
}

template <class DataType>
LinkList<DataType>:: LinkList(DataType a[], int n) {
    // 初始化头节点
    this->first= new Node<DataType>;
    this->first->next = NULL;

    // 构建业务节点,采用的 头插法
    for(int i = 0; i < n; i++)
    {
        Node<DataType> *s = new Node<DataType>;
        s->data = a[i];
        s->next = first->next;
        first->next = s;
    }
    
}

template <class DataType>
LinkList<DataType>::~LinkList()
{
    while (first != NULL)
    {
        Node<DataType> *q = first;
        first = first->next;
        delete q;
    }
}

template <class DataType>
int LinkList<DataType>::Length(){
    Node<DataType> *p = first->next;
    int count = 0;
    while(p!=NULL){
        p = p->next;
        count++;
    }
    return count;
}

template <class DataType>
DataType LinkList<DataType>:: Get(int i){
    Node<DataType> *p = first->next;
    int count = 1;
    while(p!= NULL&& count < i){
        p = p->next;
        count++;
    }
    if (p == NULL) {
        throw "Location";
    }
    return p->data;
}

template <class DataType>
int LinkList<DataType>::Locate(DataType x){
    Node<DataType> *p = first->next;
    int count = 1;
    while(p != NULL){
        
        if (p->data == x) {
            return count;
        } else {
            p = p->next;
            count++;
        }
    }
    return 0;
}

template <class DataType>
void LinkList<DataType>::Insert(int i, DataType x){
    Node<DataType> *p = first;
    int count = 0;
    // 需要找到 i-1,然后在后面的位置插入
    while(p != NULL && count < i-1){
        p = p->next;
        count++;
    }

    if (p == NULL) {
        throw "location";
    } else {
        Node<DataType> *s = new Node<DataType>;
        s->data = x;
        s->next = p->next;
        p->next = s;
    }
}

template <class DataType>
DataType LinkList<DataType>::Delete(int i){
    Node<DataType> *p = first;
    int count = 0;
    // 需要找到 i-1,然后在后面的位置插入
    while (p != NULL && count < i - 1)
    {
        p = p->next;
        count++;
    }

    if (p == NULL || p->next == NULL)
    {
        throw "location";
    }
    else
    {
        // p是要删的之前的那个节点
        Node<DataType> *s = p->next; //要删除的
        DataType x = s->data;
        Node<DataType> *temp = s->next; //要链接的
        p->next = temp;
        delete s;
        return x;
    }
}

template <class DataType>
void LinkList<DataType>::PrintList()
{
    Node<DataType> *p = first->next;
    while (p != NULL)
    {
        cout << p->data << endl;
        p = p->next;
    }
}



#endif
