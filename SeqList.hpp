#ifndef SeqList_hpp
#define SeqList_hpp

#include <iostream>
using namespace std;
const int MaxSize = 100;

template <class DataType>
class SeqList
{
  public:
    SeqList() { length = 0; }       //无参数构造方法
    SeqList(DataType a[], int n);   //有参数构造方法
    ~SeqList() {}                   //析构函数
    int Length() { return length; } //线性表长度
    DataType Get(int i);            //按位查找
    int Locate(DataType x);         //按值查找
    void Insert(int i, DataType x); //插入
    DataType Delete(int i);         //删除
    void PrintList();               //遍历
  public:
    DataType data[MaxSize]; //顺序表使用数组实现
    int length;             //存储顺序表的长度
};

template <class DataType>
SeqList<DataType>::SeqList(DataType a[], int n)
{
    if (n > MaxSize)
        throw "wrong parameter";
    for (int i = 0; i < n; i++)
        data[i] = a[i];
    length = n;
}

template <class DataType>
DataType SeqList<DataType>:: Get(int i) 
{
    if (i < 1 || i > length)
    {
        throw "error";
    } else {
        return data[i - 1];
    }
}

template <class DataType>
int SeqList<DataType>::Locate(DataType x) {
    for(int i = 0; i < length; i++)
    {
        if (data[i] == x) {
            return i+1;
        }
    }
    return 0;
}

template <class DataType>
void SeqList<DataType>:: Insert(int i, DataType x){
    if (length >= MaxSize)
        throw "Overflow";
    if (i < 1 || i > length + 1)
        throw "Location";
    for (int j = length; j >= i; j--) {
        data[j] = data[j - 1];
    }
    
    data[i - 1] = x;
    length++;
}

template <class DataType>
DataType SeqList<DataType>::Delete(int i)
{
    int x;
    if (length == 0)
        throw "Underflow";
    if (i < 1 || i > length)
        throw "Location";
    x = data[i - 1];
    for (int j = i; j < length; j++)
        data[j - 1] = data[j];
    length--;
    return x;
}

template<class DataType> void SeqList<DataType>::PrintList()
{
    for (int i = 0; i < length; i++)
        cout << data[i] << endl;
}



#endif